const express = require("express");
const Db = require("./db");

let Mongo;

async function getEmployee(_req, res) {
  if (!Db.initDone) await Db.init();
  if (!Mongo) Mongo = require("./db");
  const employees = await Mongo.employee.load();
  res.json(employees);
}
async function saveEmployee(req, res) {
  if (!Db.initDone) await Db.init();
  if (!Mongo) Mongo = require("./db");
  const { id, name } = req.body;
  await Mongo.employee.save({ id, name });
  res.json({ saved: true, id });
}
const app = express().use(express.json());
app.get("/get", getEmployee);
app.post("/save", saveEmployee);

async function closeDb() {
  console.log("app.closedb.called");
  if (!Mongo) {
    console.log("--- in if ");
    Mongo = require("./db");
    
  }
  if (Mongo.close) {
    console.log("db close exists");
    await Mongo.close();
  }
}
module.exports = { app, closeDb };
