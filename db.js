const { MongoClient } = require("mongodb");

let dbInst;
let mclient;

class DbConn {
  static employee;
  static async init() {
    if (!mclient) {
      mclient = await MongoClient.connect(`mongodb://localhost:27017/?w=1`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
    }
    dbInst = mclient.db("testdb");
    DbConn.employee = new Employee(dbInst);
    console.log("init done.");
  }
  static async close() {
    if (mclient) {
      await mclient.close(true);
      console.log("db.close");
    }
    console.log("close func called");
  }
}
class Employee {
  constructor(db) {
    this.db = db;
    this.col = db.collection("employee");
  }
  async load() {
    const emps = await this.col.find().toArray();
    return emps.map((e) => {
      const { _id: id, name } = e;
      return { id, name };
    });
  }
  async save(employee) {
    const { name, id } = employee;
    await this.col.updateOne({ _id: id }, { $set: { name } }, { upsert: true });
  }
}

module.exports = DbConn;
