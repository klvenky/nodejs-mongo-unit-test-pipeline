const { app, closeDb } = require("./app");
const Db = require("./db");

let stopServer;
const main = async () => {
  await Db.init();
  app.listen(3000, () => {
    console.log("listening on 3000");
  });
};
async function close() {
  if (stopServer) stopServer();
  await closeDb();
}

main().catch((e) => console.error(e));
process.once("SIGINT", close);
process.once("SIGTERM", close);
