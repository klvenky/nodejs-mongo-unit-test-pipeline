const request = require("supertest");

const { app, closeDb } = require("./app");

let server;

beforeAll((done) => {
  server = app.listen(4000, (err) => {
    if (err) return done(err);
    return done();
  });
});

afterAll(async function (done) {
  console.log(" --- after all ");
  await closeDb();
  await done();
});

describe("Employee API", () => {
  test("save user", async () => {
    const response = await request(app)
      .post(`/save`)
      .send({ id: 1, name: "Sample" });
    console.log(response.body);
    expect(response.status).toEqual(200);
    expect(response.body.saved).toEqual(true);
    // expect(response.body).toHaveProperty("count");
    // expect(response.body).toHaveProperty("rows");
  });
  test("SUCCESS RESULT", async () => {
    const response = await request(app).get(`/get`).send();
    expect(response.status).toEqual(200);
    expect(response.body.length).toBeGreaterThan(0);
    // expect(response.body).toHaveProperty("count");
    // expect(response.body).toHaveProperty("rows");
  });
});
